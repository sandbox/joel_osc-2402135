Path i18n Args
==============

WARNING: This module is under heavy construction, it is not recommended to use it at this time.

MOTIVATION


Currently when developing a mutlilingual site there is no way of creating path aliases with wildcards such that a secondary language alias can be created for paths with arguments. For example, if my search url is:

search/[key]

In my path aliases I have a French alias for:

(FR) recherche/ => search/

But I don't have the ability create an alias that will looks like:

recherche/*

This module will attempt to provide that support using hook_url_inbound_alter...but since this hook can be a bit heavy it will be important to implement it in a way that has the least impact to the performance of the system.
