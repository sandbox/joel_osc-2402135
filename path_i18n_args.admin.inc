<?php

/**
 * @file
 * Admin page callbacks for the subpathauto module.
 */

/**
 * Form builder; Configure the sub-path URL alias settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function path_i18n_args_settings_form($form, &$form_state) {
  $trigger_paths = variable_get('path_i18n_args_paths', NULL);
  if (!empty($trigger_paths)) {
    $paths = array();
    foreach ($trigger_paths as $row) {
      $paths[] = $row;
    }
  }

  $form['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths to redirect'),
    '#description' => t('Enter the paths (one per line) with wildcards'),
    '#default_value' => implode("\n", $paths),
  );

  return system_settings_form($form);
}

function path_i18n_args_settings_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['paths'])) {
    $paths = explode("\n", $form_state['values']['paths']);
    $form_state['values']['paths'] = $paths;
  }
}
